from django.forms import ModelForm
from .models import *

class rform(ModelForm):
    class Meta:
        model = Store
        fields = ['name', 'address', 'avatar']