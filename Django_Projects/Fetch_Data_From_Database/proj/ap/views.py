from django.contrib.auth import authenticate, login
from django.contrib.auth.models import  User
from django.shortcuts import render

from .forms import *


def register(request):
    if request.method == "GET":
        form = rform()
        return render(request, 'form.html',{"form": form})
        
    elif request.method == "POST":
        form = rform(request.POST)
        if form.is_valid():
            return render(request, 'form_submitted.html')
     

def show(request):        
    data = Store.objects.all()
    stu = {
    "student_number": data
     }
    return render(request, "show.html", stu)    
      