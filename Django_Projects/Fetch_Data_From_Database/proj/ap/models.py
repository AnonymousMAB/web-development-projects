from django.db import models

# Create your models here.
from django.db import models

class Store(models.Model):
    
    
   #id = models.AutoField(primary_key=True)# Added by default, not required explicitly
   name = models.CharField(max_length=30)
   address = models.CharField(max_length=30)
   avatar = models.CharField(max_length=10)
   interest = models.CharField(max_length=10)