from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.conf import settings
from django.http import HttpResponseRedirect
from .forms import * 
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.backends import BaseBackend
from django import forms




@login_required(login_url='/abc/')
def yo(request):
    return render(request, 'temp.html')
   


def get_name(request, pagename):


    if request.method == 'POST':
      
        form = NameForm(request.POST)
       
        if form.is_valid():


            username = request.POST.get('username')
            password = request.POST.get('password')
  
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return render(request, 'user_exists.html')
            else:

              
                return render(request, 'no_user.html')

    else:
        form = NameForm()
        context = {'form': form}
        return render(request, 'form.html', context)



def log_in(request):
    if request.method == 'POST':
        form = NameForm(request.POST)        
        if form.is_valid():         
            username = request.POST.get('username')
            password = request.POST.get('password')  
            user = authenticate(request, username=username, password=password)
        
            if user is not None:

                login(request, user)
                return render(request, 'temp.html')
                
            else:


                return render(request, 'no_user.html')  
        else:

            form = NameForm()
            context = {'form': form}
            return render(request, 'form.html', context)

    else:

        form = NameForm()
        context = {'form': form}
        return render(request, 'form.html', context)

