from django import forms
from django.contrib.auth import authenticate, login, logout, get_user_model
#from django.contrib.auth.forms import UserLoginForm

class NameForm(forms.Form):
    username = forms.CharField(label = 'username', max_length = 20)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)
