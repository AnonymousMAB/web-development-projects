from django.contrib.auth import login
from django.shortcuts import redirect, render
from .forms import *


def register(request):
    if request.method == "GET":
        return render(request, 'form.html',{"form": UserCreationForm})
        
    elif request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/nex/')


def submit(request):
    return render(request,'form_submitted.html',{})        