from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.conf import settings
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from . forms import * #imports all functions and classes from forms
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.backends import BaseBackend
from django import forms
from django.contrib import auth
def get_name(request, pagename):
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():

            #the form has 2 fields being the password and username you can also call them attributes
            username = request.POST.get('username')
            password = request.POST.get('password')
  
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return render(request, 'user_exists.html')
            else:
                 #Return an 'invalid login' error message.
                 return render(request, 'no_user.html')
            
         
         #return redirect('/authenticate/')

    #if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()
        context = {'form': form}
    return render(request, 'form.html', context)
#getname creates and redirects to forms

#don't use my_view for now
# myview performs authentication    
"""
def my_view(request):
    #the form has 2 fields being the password and username you can also call them attributes
    username = request.POST.get('username')
    password = request.POST.get('password')
  
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return render(request, 'user_exists.html')
        
    else:
        # Return an 'invalid login' error message.
         return render(request, 'no_user.html')

"""