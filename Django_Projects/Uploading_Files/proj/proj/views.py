from django.shortcuts import render, redirect
from .forms import *
from .somewhere import *


def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return redirect('/uploaded/')
    else:
        form = UploadFileForm()
    return render(request, 'form.html', {'form': form})

def file_up_success(request):
    return render(request, 'file_uploaded.html')